import { Component, OnInit, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'ap-card',
  templateUrl: './card.component.html',
})
export class CardComponent implements OnInit {

  @Input() title: String = '';

  constructor() { }
  ngOnInit()    { }

}
