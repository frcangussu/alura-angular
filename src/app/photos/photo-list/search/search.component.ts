import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'ap-search',
    templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit, OnDestroy{


    @Output() aoDigitar: EventEmitter<string>  = new EventEmitter<string>();
    @Input() value: string = ''; // usado para limpar o campo "seach"

    debounce: Subject<string> = new Subject<string>();

    ngOnInit(){
        
        // aplica o filtro usando rxjs com time (delay) 300 ms
        // no photo-list.component.html preciso disparar "debounce.next()" que funciona como se fosse um listener
        // o ".next(...)" dispara e o ".subscribe(...)" intercepta/escuta
        this.debounce
        .pipe(debounceTime(300))
        .subscribe(filter => this.aoDigitar.emit(filter));
        // .subscribe(filter => this.filter = filter);
    }

    ngOnDestroy(): void {

        // o metodo "subscribe" do debounce mantém o objeto eternamente na memória
        // portanto para liberar este espaço de memória é preciso realizar o unsubscribe
        this.debounce.unsubscribe();
      }
    

}