import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';
@Component({
  selector: 'ap-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit{

  photos: Photo[] = [];
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;
  userName: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private photoService: PhotoService
  ) { }

  ngOnInit(): void {

    this.userName = this.activatedRoute.snapshot.params['userName'];

    // recupera o valor do resolver photo-list.resolver.ts (ver a propriedade "resolve" app.rouring.module.ts)
    this.photos = this.activatedRoute.snapshot.data['photos'];

  }

  load(){
    this.photoService.listFromUserPaginated(this.userName,++this.currentPage)
    .subscribe(photos => {

      this.filter = ''; // usado para limpar o campo "seach"
      
      // >>>> não usar este pois o angular não reconhece o bind
      // this.photos.push(...photos);

      // >>>> usar este pois o bind é disparado no ato da criação de um novo objeto this.photos
      this.photos = this.photos.concat(photos);
      
      if (!photos.length) 
        this.hasMore = false;
    });
  }

}
